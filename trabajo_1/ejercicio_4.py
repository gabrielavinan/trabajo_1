#Sentencias de asignación
#___author___"Gabriela Viñan"
#___ email___ "gabriela.vinan@unl.edu.ec"
ancho = 17
alto = 12.0
uno=ancho/2
dos=ancho/2.0
tres=alto/3
cuatro=(uno+dos)*5
print("valores\t tipo variable")
print(uno,"\t",type(uno))
print(dos,"\t",type(dos))
print(tres,"\t",type(tres))
print(cuatro,"\t",type(cuatro))